---
layout: default
title: My projects
---

# Active

## Kaidan

XMPP client aiming to be user-friendly and powerful.
Written using QtQuick + Kirigami for the UI and QXmpp for the XMPP backend.

I started it in 2016 and am still working on it.

-- [https://www.kaidan.im/](https://www.kaidan.im/)

## QXmpp

Qt-based XMPP library, focus on simple design and easy usage.
I became lead developer in 2019.

-- [https://qxmpp.org](https://qxmpp.org)

## PlasmaTube

Kirigami based YouTube app, with Invidious account integration and subscription feed.
Includes a Qt-based library for the Invidious REST API.

Side project from 2019 which would deserve more attention from me. :)

-- [https://invent.kde.org/plasma-mobile/plasmatube](https://invent.kde.org/plasma-mobile/plasmatube)

## YTMX

Simple CLI tool using yt-dlp and kid3 to download and tag music from YouTube.

-- [https://codeberg.org/lnj/ytmx/](https://codeberg.org/lnj/ytmx/)


# Games

## Minetest Mod: Storage Drawers

Mod adding new item storages

-- [https://content.minetest.net/packages/LNJ/drawers/](https://content.minetest.net/packages/LNJ/drawers/)
