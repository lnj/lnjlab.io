---
layout: post
title: "Plasma Mobile Sprint 2019"
summary: "What I did at the first Plasma Mobile sprint."
date: 3019-02-10 18:00:00
tags: [KDE]
---

This was the first [Plasma Mobile sprint][sprint] and it also was my first 
sprint. It took place directly after 
the FOSDEM. Fortunately this was in my school holidays and the location also 
was in Berlin (sponsored by [Endocode AG][endocode]), so I could 
attend without problems. :)

[![Day & night at the 
sprint](day-and-night-at-the-
sprint.small
.jpg)](day-and-night-at-the-
sprint.full
.jpg)

###### View from the sprint location during day & night -- Linus Jahn, CC BY 4.0

## Monday

Most of us only arrived in the evening, so this was basically a non-coding day. 
After picking up [Ilya][ilya] from the airport, we did some Berlin sight 
seeing together with [Jonah][jbb]. In the late afternoon, when the first 
others arrived we started to code a bit, but nothing worth telling happened.

## Tuesday

On Tuesday we started to discuss the current state of Plasma Mobile and what we 
need to fix and change. The


[sprint]: https://community.kde.org/Sprints/Plasma_Mobile/2019
[endocode]: https://endocode.com/
[ilya]: https://ilyabiz.com/
[jbb]: https://jbbgameich.github.io/
