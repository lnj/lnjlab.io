---
layout: post
title: New comment system
date: 2021-05-25 14:00:00 +02:00
---

Hello, I just added a comment system to my blog. Feel free to try it out below. :)
Thanks go to [JBB](https://jbbgameich.github.io/) for writing & hosting the system.
It's privacy friendly, requires no login or email verfication and gives you the possibility to delete your posts.

