---
layout: post
title: "QLI-Installer: Installing official Qt-builds from the command line"
summary: "I wrote a python script to replace the graphical Qt installer on servers."
date: 2018-12-23 19:47:00
tags: [Qt]
---

<b>NOTE: This script is outdated. Have a look at the well-maintained fork of it: [aqtinstall][aqtinstall]</b>

<hr/>

As you probably know, Qt provides many builds for different systems and
architectures. They can be very useful, if you want to develop with the latest
libraries, even if your distro doesn't provide them (or if you're using
Windows/Mac and you got no Qt at all). In my I case I wanted to use the latest
Qt on an LTS CI system. I know that there are PPAs for Ubuntu, however, if you
want to use the Qt for Android builds, there's really no other way: either you
compile everything yourself or you need to use the official installer.

There's just one problem: Qt only provides a graphical installer, which can't be
on a server. This has been a problem for a long time and there are some
workarounds using the Qt scripting interface with a virtual display, but that
workaround isn't that easy to use and error-prone. You can find an example of
this on [stackoverflow][hacky-workaround].

Since the Qt Installer basically only lets you select the Qt version and then
downloads & extracts everything, I thought that it couldn't be too hard to
rewrite that in a simple script. I browsed a bit through the
[Qt installer repository][installer-repo] and started to write a python script
that should output the URLs for a specific Qt version. Turns out: the URLs are
not consistent in all Qt versions and they contain long build numbers. To solve
this I needed to parse the `Updates.xml` file for each target/version. These
files list all the packages and their dependencies. From these we only need the
main package (it contains most modules as QtCore, QtSql, QtSvg and many more,
only QtCharts and similar are missing) and can ignore its dependencies (it only
depends on the qtcreator, documentation and examples, which we all don't need
for our purpose).

In the end I came up with a python script that can download and extract you
every Qt version you want (even iOS builds can be downloaded on a Linux host
system, which wasn't possible before :D). The script (currently) just calls the
p7zip commands for extracting the packages which isn't optimal, but OK.

So if you would like to use the official Qt builds on a server and don't want to
argue with the graphical installer, you might want to try my script. It's
available on my ~~GitLab instance~~ [sourcehut repo][qli-installer-gitlab].
Here is an example how to use it on debian-based systems:

```bash
sudo apt install python3-requests p7zip-full wget

wget https://git.kaidan.im/lnj/qli-installer/raw/master/qli-installer.py
chmod +x qli-installer.py

./qli-installer.py 5.12.0 linux desktop
```

This will download and extract Qt 5.12.0 to a folder called `5.12.0` in your
current directory. You can find more details in the README. If you have any
suggestions or you've found a bug (the installer repository format changes from
time to time), please [submit an issue][submit-issue].

[hacky-workaround]: https://stackoverflow.com/a/34032216/4483773
[installer-repo]: https://download.qt.io/online/qtsdkrepository/
[qli-installer-gitlab]: https://git.sr.ht/~lnj/qli-installer
[submit-issue]: https://git.kaidan.im/lnj/qli-installer/issues/new
[aqtinstall]: https://github.com/miurahr/aqtinstall
