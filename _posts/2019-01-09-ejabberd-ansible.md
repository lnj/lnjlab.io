---
layout: post
title: "The perfect ejabberd setup with ansible"
summary: "How to use my ansible role for installing the perfect XMPP server. I gave a talk about this at the <a href=\"https://de.movim.eu/?node/pubsub.movim.eu/berlin-xmpp-meetup/the-perfect-ejabberd-server-setup-using-ansible-KEMXyE\">XMPP meetup</a>."
date: 2019-01-09 12:00:00
tags: [XMPP]
---

## Previously on "XMPP server setup"...

In April last year I setup the `kaidan.im` XMPP server with ejabberd. As you would expect I did that
completely manually, so first running `apt get install ejabberd`, then editing the config file and
so on. I read the ejabberd documentation in detail to enable all useful features and to get the
nearly perfect server config. I also imported the database from a
[friend of mine](https://jbbgameich.github.io/), who hosted his server (`jbb.ddns.net`) on a
Raspberry Pi before.

Then, in July I got a small job to install an XMPP server for the [DBJR](https://dbjr.de/)
(Deutscher Bundesjugendring). It was already clear that we'll first test the XMPP server on a small
(Hetzner) cloud server and later move it to a larger one as the project grows. Besides the internal
server `chat.dbjr.org` we also set up the `yochat.eu` domain (youth organization chat) to be opened
for free registration later. Because we didn't want to do all the setup multiple times (and
researching everything after forgetting everything), I came up with the idea of using
[ansible](https://www.ansible.com/) to automate everything. The result was nice, but still very
server specific. Later I based on the ansible playbook for moving my own server. Recently I also did
some real abstraction, so now only some config options would need to be changed to get the
chat.dbjr.org-server. On 9th January, I gave a talk about this at the
[XMPP Meetup](https://de.movim.eu/?node/pubsub.movim.eu/berlin-xmpp-meetup/).

## How to setup your own server

My ansible role is publicly available on my [GitLab instance][git-ejabberd]. I'll show you how to
use it in this blog post, but we won't go into details about ansible. The first step is to create a
new ansible playbook for this project (as long as you don't have one yet). Just create this folder
structure (we'll talk about the speific files later):
```
$ ls myplaybook/ myplaybook/env/ myplaybook/env/host_vars/
myplaybook/:
env/  roles/  mysetup.yml

myplaybook/env/:
myhosts.inventory  host_vars/

myplaybook/env/host_vars/:
myxmppserver.yml
```

Then clone the ejabberd and certbot roles into the `roles` folder:
```bash
git clone ssh://git@git.kaidan.im/lnj/ansible-role-ejabberd ejabberd
git clone ssh://git@git.kaidan.im/lnj/ansible-role-certbot certbot
```

After that you should add the target server to the inventory file:
```toml
# this is the group name:
[xmppservers]
# this is the server name (as you would use it when connecting via. ssh); you
# could also write `user@xmpp.server.example`, but it's better to add that info
# to your ~/.ssh/config
myxmppserver
```

Now copy the default configurations of the roles to your host configuration
(remove the three dashes `---` from the second file):
```bash
cat roles/ejabberd/defaults/main.yml roles/certbot/defaults/main.yml > env/host_vars/myxmppserver.yml
```

You can edit that file and adjust everything to your needs. The config options should all be
self-explaining (contact me if they're not). If you need some advanced options you can just edit the
ejabberd.yml template file in the ejabberd role. So now what's still missing is an ansible script
(i.e. `mysetup.yml`) that will execute the ejabberd role on your server:
```yml
---

- name: Install ejabberd
  hosts: xmppservers # this is the group name, you could also specify a single host
  become: true
  roles:
    - certbot
    - ejabberd
```

And that's basically it, you can now execute the playbook on your server using this command:
```bash
ansible-playbook -i env/myhosts.inventory mysetup.yml
```

If you're using this, it'll probably be much easier for you to setup the server (and especially
when moving it to another machine!).

[git-ejabberd]: https://git.kaidan.im/lnj/ansible-role-ejabberd
