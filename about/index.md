---
layout: default
title: About me
---

I'm Linus, a physics student, free software enthusiast & developer.
I like strong-typed languages like C++ or Rust, but also have experiences in python.
My most extensive project to date is [Kaidan](https://kaidan.im), a convergent and
cross-platform instant messaging client using the decentralized Jabber/XMPP
network.
Since 2019 I'm also the maintainer of the underlying XMPP library [QXmpp](https://qxmpp.org).

## Contact me

* Jabber/XMPP: [lnj@kaidan.im](https://i.kaidan.im/#lnj@kaidan.im)
* E-Mail:      [lnj@kaidan.im](mailto:lnj@kaidan.im)
* Mastodon:    [@lnj@layer8.space](https://layer8.space/@lnj)
* Coding:      [GitLab (KDE)](https://invent.kde.org/lnj),
               [Codeberg.org](https://codeberg.org/lnj),
               [GitLab.com](https://gitlab.com/lnj),
               [GitHub](https://github.com/lnj2)
